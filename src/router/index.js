import Vue from 'vue'
import Router from 'vue-router'
import white from '@/components/White'
import black from '@/components/Black'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'white',
      component: white
    },
    {
    	path: '/black',
    	name: 'black',
    	component: black
    }
  ]
})
